package com.example.muhammadahsan.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FlappyBird extends ApplicationAdapter {
	SpriteBatch batch;

	//Texture is an image type
	Texture backgroundImg;
	Texture[] birds; //A texture array

	int flapState = 0;
	float birdY = 0;
	float velocity = 0;

	int gameState = 0;

	//This is like an oncreate mtehod
	@Override
	public void create () {
		batch = new SpriteBatch();
		backgroundImg = new Texture("bg.png");

		//bird = new Texture("bird.png"); //Now instead of texture, we need a texture array
		birds = new Texture[2];
		birds[0] = new Texture("bird.png");
		birds[1] = new Texture("bird2.png");

		birdY = Gdx.graphics.getHeight() /2 - birds[0].getHeight() /2;//Copied from render method, from bird.draw because only Y position of the bird is going to be changed.

	}


	//Render method is a constant looooop
	@Override
	public void render () {

		//For touch mechanism to work
		if (Gdx.input.justTouched()){
			Gdx.app.log("Touched", "Yup!");	//Using GDX logging mechanism
		}

		velocity++;
		birdY -= velocity;
		Gdx.app.log("velocity++", String.valueOf(velocity));
		Gdx.app.log("birdY", String.valueOf(birdY));

		//Change this state means changing birds[0] to birds[1] means bird is flapping its wings
		if (flapState == 0){
			flapState =1;
		} else {
			flapState = 0;
		}

		batch.begin();
		//Strech background image to the whole game
		batch.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		//So this line of code drew the bird a little off center, because the birds below position was aligned according to starting point of bird
		//batch.draw(bird, Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() /2 );
		batch.draw(birds[flapState], Gdx.graphics.getWidth() /2 - birds[flapState].getWidth() /2, birdY);


		batch.end();


	}
	
	@Override
	public void dispose () {
		//batch.dispose();
		//img.dispose();
	}
}






























